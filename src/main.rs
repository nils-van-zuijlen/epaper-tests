#![no_std]
#![no_main]

use bsp::entry;
use defmt::*;
use defmt_rtt as _;
use embedded_hal::spi::{Mode, Phase, Polarity};
use panic_probe as _;

// Provide an alias for our BSP so we can switch targets quickly.
// Uncomment the BSP you included in Cargo.toml, the rest of the code does not need to change.
use rp_pico as bsp;
// use sparkfun_pro_micro_rp2040 as bsp;

use bsp::hal::{
    clocks::{init_clocks_and_plls, Clock},
    pac,
    fugit::RateExtU32,
    sio::Sio,
    watchdog::Watchdog,
    spi::Spi,
};


use embedded_graphics::{prelude::*, primitives::{Line, PrimitiveStyle, PrimitiveStyleBuilder}};
use epd_waveshare::{epd2in13bc::*, prelude::*};

#[entry]
fn main() -> ! {
    info!("Program start");
    let mut pac = pac::Peripherals::take().unwrap();
    let core = pac::CorePeripherals::take().unwrap();
    let mut watchdog = Watchdog::new(pac.WATCHDOG);
    let sio = Sio::new(pac.SIO);

    // External high-speed crystal on the pico board is 12Mhz
    let external_xtal_freq_hz = 12_000_000u32;
    let clocks = init_clocks_and_plls(
        external_xtal_freq_hz,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let mut delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().to_Hz());

    let pins = bsp::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    let dc = pins.gpio28.into_push_pull_output();
    let busy_in = pins.gpio27.into_floating_input();
    let cs_pin = pins.gpio26.into_push_pull_output();
    let rst = pins.gpio15.into_push_pull_output();

    let data: bsp::Gp3Spi0Tx = pins.gpio3.reconfigure();
    let clock: bsp::Gp2Spi0Sck = pins.gpio2.reconfigure();

    let mut spi = Spi::<_, _, _, 8>::new(pac.SPI0, (data, clock)).init(&mut pac.RESETS, 125_000_000_u32.Hz(), 4_000_000_u32.Hz(), Mode{polarity: Polarity::IdleLow, phase: Phase::CaptureOnFirstTransition});

    let mut epd = Epd2in13bc::new(&mut spi, cs_pin, busy_in, dc, rst, &mut delay).unwrap();

    // Use display graphics from embedded-graphics
    // This display is for the black/white/chromatic pixels
    let mut tricolor_display = Display2in13bc::default();
    // Use embedded graphics for drawing a black line
    let _ = Line::new(Point::new(0, 120), Point::new(0, 200))
    .into_styled(PrimitiveStyle::with_stroke(TriColor::Black, 1))
    .draw(&mut tricolor_display);

    // We use `chromatic` but it will be shown as red/yellow
    let _ = Line::new(Point::new(15, 120), Point::new(15, 200))
    .into_styled(PrimitiveStyle::with_stroke(TriColor::Chromatic, 1))
    .draw(&mut tricolor_display);

    // Display updated frame
    epd.update_color_frame(
        &mut spi,
        &tricolor_display.bw_buffer(),
                           &tricolor_display.chromatic_buffer()
    ).unwrap();
    epd.display_frame(&mut spi, &mut delay).unwrap();

    // Set the EPD to sleep
    epd.sleep(&mut spi, &mut delay).unwrap();

    loop {
        info!("on!");
    }
}

// End of file
